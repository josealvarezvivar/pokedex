package es.itsmejose.pokedex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Queue;

import es.itsmejose.pokedex.Models.Pokemon;
import es.itsmejose.pokedex.Models.PokemonView;
import es.itsmejose.pokedex.Utils.CustomConstants;

public class PokemonViewActivity extends AppCompatActivity {

    private ImageView ivPokemon;
    private TextView tvNamePokemonView;
    private TextView tvNumberPokemonView;
    private TextView tvHeightPokemonView;
    private TextView tvWeightPokemonView;
    private TextView tvDescriptionPokemonView;
    private Pokemon selectedPokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_view);

        initComponent();
        gettingInfoAboutSelectingPok();

    }

    private void initComponent(){
        ivPokemon = findViewById(R.id.iv_pokemon);
        tvNamePokemonView = findViewById(R.id.tv_name_pokemon_view);
        tvNumberPokemonView = findViewById(R.id.tv_number_pokemon_view);
        tvHeightPokemonView = findViewById(R.id.tv_height_pokemon_view);
        tvWeightPokemonView = findViewById(R.id.tv_weight_pokemon_view);
        tvDescriptionPokemonView = findViewById(R.id.tv_description_pokemon_view);
        selectedPokemon = getIntent().getParcelableExtra("selected_pokemon");
    }

    private void gettingInfoAboutSelectingPok(){

        String url = selectedPokemon.getUrl();
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());


        JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        PokemonView pokemonView = gettingPokemonInfo(response);
                        fillingTheView(pokemonView);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );

        queue.add(jsonRequest);
    }

    private PokemonView gettingPokemonInfo(JSONObject response){
        return new PokemonView(
                gettingStrings("name",response),
                gettingShorts("id",response),
                gettingFloats("height",response),
                gettingFloats("weight",response),
                "",
                gettingImages(response)
        );
    }


    private String gettingStrings(String key,JSONObject response){
        String stringToReturn;

        try {
            stringToReturn = response.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            stringToReturn = "";
        }

        return stringToReturn;
    }

    private short gettingShorts(String key,JSONObject response){
        short shortToReturn;

        try {
            shortToReturn = (short)response.getInt(key);
        } catch (JSONException e) {
            e.printStackTrace();
            shortToReturn = 0;
        }

        return shortToReturn;
    }

    private float gettingFloats(String key, JSONObject response){
        float floatToReturn;

        try {
            floatToReturn = (float) response.getDouble(key);
        } catch (JSONException e) {
            e.printStackTrace();
            floatToReturn = 0.0f;
        }

        return floatToReturn;
    }

    private String gettingImages(JSONObject response){
        String imageToReturn;

        try {
            JSONObject sprites = response.getJSONObject("sprites");
            imageToReturn = sprites.getString("front_default");
        } catch (JSONException e) {
            e.printStackTrace();
            imageToReturn = "";
        }

        return imageToReturn;
    }

    private void fillingTheView(PokemonView pokemonView){
        Picasso.get().load(pokemonView.getImage()).into(ivPokemon);
        tvNamePokemonView.setText(pokemonView.getName());
        tvNumberPokemonView.setText(CustomConstants.NUMBER_HASH.concat(String.valueOf(pokemonView.getNumber())));
        tvWeightPokemonView.setText(String.valueOf(pokemonView.getWeight()).concat(CustomConstants.UNIT_W));
        tvHeightPokemonView.setText(String.valueOf(pokemonView.getHeight()).concat(CustomConstants.UNIT_H));
        tvDescriptionPokemonView.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur malesuada dui et laoreet scelerisque. " +
                "Sed et augue leo. Ut non erat arcu. Aenean viverra sapien tortor, eu imperdiet nunc tristique eget. Maecenas mollis erat vitae aliquam aliquam. " +
                "Etiam ultrices, nibh ultrices lobortis volutpat, diam orci lacinia dolor, sodales suscipit velit lacus eu erat. Maecenas quis convallis enim." +
                " Phasellus imperdiet fringilla nisi, vitae sodales tortor dapibus nec. Morbi euismod elit elit, sed commodo massa convallis et. In euismod viverra " +
                "porttitor. Aliquam ex lacus, placerat at viverra ac, sodales a purus.");
    }
}
