package es.itsmejose.pokedex.Models;

public class PokemonView {

    private String name;
    private short number;
    private float height;
    private float weight;
    private String description;
    private String image;

    public PokemonView() {
    }

    public PokemonView(String name, short number, float height, float weight, String description, String image) {
        this.name = name;
        this.number = number;
        this.height = height;
        this.weight = weight;
        this.description = description;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public short getNumber() {
        return number;
    }

    public float getHeight() {
        return height;
    }

    public float getWeight() {
        return weight;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
