package es.itsmejose.pokedex.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Pokemon implements Parcelable{

    private String name;
    private String url;

    public Pokemon(){}

    public Pokemon(String name, String url){
        this.name = name;
        this.url = url;
    }

    protected Pokemon(Parcel in) {
        name = in.readString();
        url = in.readString();
    }

    public static final Creator<Pokemon> CREATOR = new Creator<Pokemon>() {
        @Override
        public Pokemon createFromParcel(Parcel in) {
            return new Pokemon(in);
        }

        @Override
        public Pokemon[] newArray(int size) {
            return new Pokemon[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(url);
    }
}
