package es.itsmejose.pokedex.Utils;

public interface CustomConstants {

    String URL_POKEAPI = "https://pokeapi.co/api/v2/pokemon/";
    String UNIT_W = "Kg";
    String UNIT_H = "cm";
    String NUMBER_HASH = "#";

}
