package es.itsmejose.pokedex.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import es.itsmejose.pokedex.Models.Pokemon;
import es.itsmejose.pokedex.R;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> implements View.OnClickListener{

    private ArrayList<Pokemon> pokemonList;
    private View.OnClickListener listener;

    public PokemonAdapter(ArrayList<Pokemon> pokemonList){
        this.pokemonList = pokemonList;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onClick(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPokemonName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPokemonName = itemView.findViewById(R.id.tv_list_pokemon);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemon_list_card, parent,Boolean.FALSE);
        myView.setOnClickListener(this);
        return new ViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvPokemonName.setText(pokemonList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return pokemonList.size();
    }
}
