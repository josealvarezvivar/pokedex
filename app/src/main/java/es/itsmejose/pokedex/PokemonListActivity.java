package es.itsmejose.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.itsmejose.pokedex.Adapters.PokemonAdapter;
import es.itsmejose.pokedex.Models.Pokemon;

public class PokemonListActivity extends AppCompatActivity {

    private RecyclerView rvPokemonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);

        rvPokemonList = findViewById(R.id.rv_pokemon);

        ArrayList<Pokemon> pokemonList = obtainingArrayIntent();
        launchingAdapter(pokemonList);
    }

    private ArrayList<Pokemon> obtainingArrayIntent(){
        Gson gson = new Gson();
        String pokemonStringList = getIntent().getStringExtra("pokemon_list");
        Type type = new TypeToken<List<Pokemon>>(){}.getType();

        return gson.fromJson(pokemonStringList,type);
    }

    private void launchingAdapter(final ArrayList<Pokemon> pokemonList){
        PokemonAdapter pokemonAdapter = new PokemonAdapter(pokemonList);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvPokemonList.setLayoutManager(linearLayoutManager);
        rvPokemonList.setAdapter(pokemonAdapter);

        pokemonAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = rvPokemonList.getChildAdapterPosition(view);
                Pokemon selectedPokemon = pokemonList.get(position);
                launchViewActivity(selectedPokemon);
            }
        });
    }

    private void launchViewActivity(Pokemon pokemon){
        Intent intent = new Intent(PokemonListActivity.this,PokemonViewActivity.class);
        intent.putExtra("selected_pokemon",pokemon);
        startActivity(intent);
    }
}
