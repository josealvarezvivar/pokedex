package es.itsmejose.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.itsmejose.pokedex.Models.Pokemon;
import es.itsmejose.pokedex.Utils.CustomConstants;

public class SplashScreen extends AppCompatActivity {

    private ImageView ivPokeball;
    private ArrayList<Pokemon> pokemons;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        initComponents();
        startAnimation();
        downloadingPokemon();
    }

    private void initComponents(){
        ivPokeball = findViewById(R.id.iv_pokeball);
    }

    private void startAnimation(){
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_anim);
        ivPokeball.setAnimation(animation);
    }

    private void downloadingPokemon(){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, CustomConstants.URL_POKEAPI, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    pokemons = gettingThePokemons(response);

                    if(pokemons!=null){
                        launchPokemonListActivity(pokemons);
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
        });

        queue.add(jsonRequest);
    }


    private ArrayList<Pokemon> gettingThePokemons(JSONObject response){

        ArrayList<Pokemon> pokemonList;

        try {
            pokemonList = new ArrayList<>();
            JSONArray pokemonArray = response.getJSONArray("results");

            for(int i=0; i < 151; i++){
                JSONObject currentPokemon = pokemonArray.getJSONObject(i);
                pokemonList.add(new Pokemon(currentPokemon.getString("name"),currentPokemon.getString("url")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            pokemonList = null;
        }

        return pokemonList;
    }

    private void launchPokemonListActivity(ArrayList<Pokemon> pokemonList){
        Gson gson = new Gson();
        String pokemonStringList = gson.toJson(pokemonList);

        Intent intent = new Intent(SplashScreen.this,PokemonListActivity.class);
        intent.putExtra("pokemon_list",pokemonStringList);
        startActivity(intent);
    }
}
